function renderElement(){
    let themsColor = localStorage.getItem("themsColor");
    let bodyColor = document.querySelector('body');
    bodyColor.style.background = themsColor == 'first' ? '#c9eac2' : '#ead9c2';

    let btnChange = document.querySelector('.change_btn');

    btnChange.addEventListener('click',() => {
        let bodyColor = document.querySelector('body');
        bodyColor.style.background = '#c9eac2';
        localStorage.setItem("themsColor", "first");
    })

    btnChange.addEventListener('dblclick',() => {
        let bodyColor = document.querySelector('body');
        bodyColor.style.background = '#ead9c2';
        localStorage.setItem("themsColor", "second");
    })
}

renderElement()